# Descomplicando o GitLab

Treinamento LinuxTips para a certificação GitLab - Descomplicando o GitLab

### Day-1

'''bash
	- Entendemos o que é o Git
	- Entendemos o que é Gitlab
	- Entendemos o que é Working Dir, Index e Head
	- Como criar um grupo no GitLab
	- Como criar um repositório Git
	- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
	- Como criar uma branch
	- Como criar um Merge Request
	- Como adicionar um membro no projeto
	- Como fazer Marge na Master/Main
	- Como associar um repositório local com um repositório remoto
	- Como importar um repositório do Github para o GitLab
	- Mudamos a branch padrão para Main

'''	
